package edu.sjsu.android.loaderapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button buttonCall;
    Button buttonWeb;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonCall = findViewById(R.id.phone_button);
        buttonWeb = findViewById(R.id.web_button);

        buttonCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intentCall = new Intent(Intent.ACTION_DIAL);
                intentCall.setData(Uri.parse("tel:194912344444"));
                startActivity(intentCall);
            }
        });
        buttonWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intentWeb = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.amazon.com"));
                //Dont hardcode this, use string xml later on CHANGE
                String title = "Choose a Browser";
                Intent chooser = Intent.createChooser(intentWeb, title);
                startActivity(chooser);

            }
        });

    }
}